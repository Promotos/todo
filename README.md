# Todo
Web application for daily todos.

## Install
### Release
Download a release and execute the run.bat / run.sh
### Development
- Require >= Go 1.11
- Require revel - https://github.com/revel/revel
- Require buntdb - https://github.com/tidwall/buntdb
Development run: revel run -a bitbucket.org/Promotos/todo


## Help

* The [Getting Started with Revel](http://revel.github.io/tutorial/gettingstarted.html).
* The [Revel guides](http://revel.github.io/manual/index.html).
* The [Revel sample apps](http://revel.github.io/examples/index.html).
* The [API documentation](https://godoc.org/github.com/revel/revel).
