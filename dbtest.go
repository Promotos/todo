package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/tidwall/buntdb"
)

type Todo struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
	Desc string `json:"desc"`
}

func main() {
	db, err := buntdb.Open("data.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.CreateIndex("todos", "*", buntdb.IndexString)

	todo := &Todo{
		ID:   12,
		Name: "Test Name",
		Desc: "Some desc",
	}

	db.Update(func(tx *buntdb.Tx) error {
		data, _ := json.Marshal(todo)
		tx.Set("todo:0", string(data), nil)
		return nil
	})

	db.View(func(tx *buntdb.Tx) error {
		tx.Ascend("todos", func(key, val string) bool {
			todo := Todo{}
			fmt.Println(key + "=" + val)
			json.Unmarshal([]byte(val), &todo)
			fmt.Println(todo.Name)
			return true
		})
		return nil
	})
}
