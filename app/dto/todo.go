package dto

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"bitbucket.org/Promotos/todo/app"
	"github.com/tidwall/buntdb"
)

// Todo data structure
type Todo struct {
	ID            int64  `json:"id"`
	State         int    `json:"state"`
	Name          string `json:"name"`
	Desc          string `json:"desc"`
	ActiveMinutes int    `json:"activeMinutes"`
}

// State const values
const (
	Open = iota
	Active
	Closed
)

var (
	ErrArgEmpty = errors.New("The argument must not be empty")
)

// NewTodo creates a new todo instance with the name and description.
// The ID is generated
func NewTodo(name, desc string) (Todo, error) {
	if len(name) == 0 {
		return Todo{}, ErrArgEmpty
	}
	return Todo{
		ID:    makeTimestamp(),
		State: Open,
		Name:  name,
		Desc:  desc,
	}, nil
}

// SaveTodo is used to save a given todo element
func SaveTodo(todo Todo) error {
	return app.DB.Update(func(tx *buntdb.Tx) error {
		data, _ := json.Marshal(todo)
		tx.Set(makeKey(todo.ID), string(data), nil)
		return nil
	})
}

// AllTodos is used to get a slice of all stored todos
func AllTodos() ([]Todo, error) {
	todos := make([]Todo, 0)
	err := app.DB.View(func(tx *buntdb.Tx) error {
		tx.Descend("todos", func(key, val string) bool {
			todo := Todo{}
			json.Unmarshal([]byte(val), &todo)
			todos = append(todos, todo)
			return true
		})
		return nil
	})
	return todos, err
}

// GetTodo is used to get one todo by id
func GetTodo(id int64) Todo {
	todo := Todo{}
	app.DB.View(func(tx *buntdb.Tx) error {
		val, _ := tx.Get(makeKey(id), true)
		json.Unmarshal([]byte(val), &todo)
		return nil
	})

	return todo
}

func makeKey(id int64) string {
	return "todo:" + strconv.FormatInt(id, 10)
}

func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}
