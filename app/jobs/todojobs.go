package jobs

import (
	"bitbucket.org/Promotos/todo/app/dto"
	"github.com/revel/modules/jobs/app/jobs"
	"github.com/revel/revel"
)

type todoActiveJob struct{}

func (c todoActiveJob) Run() {
	todos, err := dto.AllTodos()
	if err != nil {
		revel.AppLog.Error(err.Error())
	}
	for _, todo := range todos {
		if todo.State == dto.Active {
			todo.ActiveMinutes++
			err := dto.SaveTodo(todo)
			if err != nil {
				revel.AppLog.Error(err.Error())
			}
		}
	}
}

func init() {
	revel.OnAppStart(func() {
		jobs.Schedule("@every 1m", todoActiveJob{})
	})
	revel.AppLog.Info("Job scheduled")
}
