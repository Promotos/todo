package controllers

import (
	"strconv"

	"bitbucket.org/Promotos/todo/app/dto"
	"bitbucket.org/Promotos/todo/app/routes"
	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
	todosOpen := make([]dto.Todo, 0)
	todosActive := make([]dto.Todo, 0)
	todosClosed := make([]dto.Todo, 0)
	todos, err := dto.AllTodos()
	if err != nil {
		c.Flash.Error(err.Error())
		c.Validation.Keep()
		c.FlashParams()
	} else {
		for _, todo := range todos {
			switch todo.State {
			case dto.Open:
				todosOpen = append(todosOpen, todo)
			case dto.Active:
				todosActive = append(todosActive, todo)
			case dto.Closed:
				todosClosed = append(todosClosed, todo)
			}
		}
	}

	return c.Render(todosOpen, todosActive, todosClosed)
}

func (c App) New() revel.Result {
	return c.Render()
}

func (c App) PostNew(name, desc string) revel.Result {
	c.Validation.Required(name).Message("The name is required.")
	c.Validation.MinSize(name, 5).Message("Name must have at least 5 chars.")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.App.New())
	}

	todo, err := dto.NewTodo(name, desc)
	if err != nil {
		c.Flash.Error(err.Error())
		c.Validation.Keep()
		c.FlashParams()
	} else {
		dto.SaveTodo(todo)
	}

	return c.Redirect(routes.App.Index())
}

func (c App) Activate(id int64) revel.Result {
	todo := dto.GetTodo(id)
	todo.State = dto.Active
	dto.SaveTodo(todo)
	return c.Redirect(routes.App.Index())
}

func (c App) Complete(id int64) revel.Result {
	todo := dto.GetTodo(id)
	todo.State = dto.Closed
	dto.SaveTodo(todo)
	return c.Redirect(routes.App.Index())
}

func (c App) Edit(id int64) revel.Result {
	todo := dto.GetTodo(id)
	return c.Render(todo)
}
func (c App) PostEdit(id, name, desc string) revel.Result {
	i, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		c.Flash.Error(err.Error())
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.App.Index())
	} else {

		c.Validation.Required(name).Message("The name is required.")
		c.Validation.MinSize(name, 5).Message("Name must have at least 5 chars.")

		if c.Validation.HasErrors() {
			c.Validation.Keep()
			c.FlashParams()
			return c.Redirect(routes.App.Edit(i))
		} else {
			todo := dto.GetTodo(i)
			todo.Name = name
			todo.Desc = desc
			dto.SaveTodo(todo)
		}
	}

	return c.Redirect(routes.App.Index())
}
